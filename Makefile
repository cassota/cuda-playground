
CC=clang
CFLAGS=-O2 -g -pg
LIBS=-lm

cpu:
	$(CC) $(CFLAGS) add.c $(LIBS)

gpu:
	nvcc add.cu


clean:
	$(RM) *.out *.o

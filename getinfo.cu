#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    int n;
    int devId;
    cudaGetDevice(&devId);
    cudaDeviceGetAttribute(&n, cudaDevAttrMultiProcessorCount, devId);

    printf("Device ID: %d\n", devId);
    printf("Number of treaming multiprocessors: %d\n", n);
    return 0;
}

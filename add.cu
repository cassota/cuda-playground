// author: sept 2020
// cassio batista - https://cassota.gitlab.io
// source: https://developer.nvidia.com/blog/even-easier-introduction-cuda/
// TIP: to calculate the bandwidth: Bytes / Seconds

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//__global__ void
//init(int n, float* x, float* y)
//{
//    int i;
//    int index = threadIdx.x + blockIdx.x * blockDim.x;
//    int stride = blockDim.x * gridDim.x;
//    for (i = index; i < n; i += stride) {
//        x[i] = 1.0f;
//        y[i] = 2.0f;
//    }
//}

// Function to add the elements of two arrays.
// Make it a CUDA kernel function, i.e., a function the GPU can run.
// __global__: specifier that tells NVCC this functions runs on GPU and
//             can be called from CPU code.
__global__ void
add(int n, float* x, float* y)
{
    int i;
    // gridDim: number of blocks in the grid
    // blockDim: number of threads per block
    // blockIdx: index of the current block
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;
    // grid-stride loop
    for(i = index; i < n; i += stride) {
        y[i] = x[i] + y[i];
    }
}

int
main(int argc, char** argv)
{
    if(argc != 2) {
        printf("usage: %s <BLOCK_SIZE>\n", argv[0]);
        printf("  <BLOCK_SIZE> is the number of threads as a multiple of 32.\n");
        return EXIT_FAILURE;
    }

    int i;
    int N = 1<<20; // 1M elements
    int block_size;
    if (!(block_size = atoi(argv[1]))) {
        printf("Error: Bad arg: '%s'\n", argv[1]);
        exit(1);
    }

    int num_blocks = (N + block_size - 1) / block_size;
    float max_error = 0.0;

    // Allocate Unified Memory (UM) -- accessible from CPU or GPU
    float* x;
    float* y;
    cudaMallocManaged(&x, N * sizeof(float));
    cudaMallocManaged(&y, N * sizeof(float));

    // initalise x and y arrays on the host 
    // NOTE: page faults are occurring because this initialisation is performed
    // on the host instead of the device.
    // see https://developer.nvidia.com/blog/unified-memory-cuda-beginners/
    for(i = 0; i < N; i++) {
        x[i] = 1.0f;
        y[i] = 2.0f;
    }

    // Run kernel on 1M elements on the CPU
    // Launch 'add()' kernel on GPU by specifying the triple angle bracket,
    // operator, which is here called 'execution configuration'.
    // NOTE: Kernels use blocks of threads multiple of 32 in size
    // <<<X, Y>>> lauching X blocks with Y threads each
    printf("Adding %d integers up (~%d MB) using %d blocks of %d threads\n",
            N, (int)((N * 4 * 2) / 1024 / 1024), num_blocks, block_size);
    add<<<num_blocks, block_size>>>(N, x, y);

    // Wait for the GPU to finish before accessing on host
    cudaDeviceSynchronize();

    // Check for erros (all values should be 3.0f)
    for(i = 0; i < N; i++) {
        max_error = fmax(max_error, fabs(y[i] - 3.0));
    }
    printf("max error: %f\n", max_error);

    // Free memory
    cudaFree(x);
    cudaFree(y);

    return EXIT_SUCCESS;
}

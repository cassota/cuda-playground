#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Function to add the elements of two arrays.
void
add(int n, float* x, float* y)
{
    int i;
    for(i = 0; i < n; i++) {
        y[i] = x[i] + y[i];
    }
}

int
main(void)
{
    int i;
    int N = 1<<20; // 1M elements
    float* x = (float*)malloc(N * sizeof(float));
    float* y = (float*)malloc(N * sizeof(float));
    float max_error = 0.0;

    // initalise x and y arrays on the host
    for(i = 0; i < N; i++) {
        x[i] = 1.0f;
        y[i] = 2.0f;
    }

    // Run kernel on 1M elements on the CPU
    add(N, x, y);

    // Check for erros (all values should be 3.0f)
    for(i = 0; i < N; i++) {
        max_error = fmax(max_error, fabs(y[i] - 3.0));
    }
    printf("max error: %f\n", max_error);

    // Free memory
    free(x);
    free(y);

    return EXIT_SUCCESS;
}
